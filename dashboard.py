import json
import requests
import sys
import base64
import os
import datetime
from copy import deepcopy

WIDGETS_PER_LINE = 1

# x_offset = 560
x_offset = 400
y_offset = 160

host = ''
port = ''
user = ''
password = ''
account = ''
token = ''
cookies = ''
importacao = 0
BTReplace = "Login"

def get_auth(host, port, user, password, account):
    url = '{}:{}/controller/auth'.format(host, port)
    auth2 = user + "@" + account + ":" + password
    headers = {
        'Authorization': 'Basic ' + base64.b64encode(auth2.encode('UTF-8')).decode('ascii')  
    }
    params = (
        ('action', 'login'),
    )
    response = requests.get(url, headers=headers, params=params)
    global token
    global cookies
    cookies = response.cookies 
    token = response.cookies.get("X-CSRF-TOKEN")

    return 0

def get_dashboards(host, port, user, password, account):
    url = '{}:{}/controller/restui/dashboards/getAllDashboardsByType/false'.format(host, port)
    params = {'output': 'json'}
    auth2 = user + "@" + account + ":" + password
    headers = {
        'Authorization': 'Basic ' + base64.b64encode(auth2.encode('UTF-8')).decode('ascii') ,
        'X-CSRF-TOKEN' : token
    }
    r = requests.get(url, params=params, headers=headers, cookies=cookies)
    #print(r)
    
    return sorted(r.json(), key=lambda k: k['name'])


def get_applications(host, port, user, password, account):
    url = '{}:{}/controller/rest/applications'.format(host, port)
    auth = ('{}@{}'.format(user, account), password)
    #print(auth)
    params = {'output': 'json'}

    #print('Getting apps', url)
    r = requests.get(url, auth=auth, params=params)
    return sorted(r.json(), key=lambda k: k['name'])

def find_dashboard(dashboards, name):
    id = 0
    if dashboards != "":
        for i in dashboards:
            if i['name'].encode("utf-8")  == name:
                id = i['id']
                break
    return id

def put_dashboard(host, port, user, password, account, dashboard):
    url = '{}:{}/controller/CustomDashboardImportExportServlet'.format(host, port)
    auth = ('{}@{}'.format(user, account), password)
    files = {
        'file': (dashboard, open(dashboard, 'rb')),
    }
    print('import dashboard apps', dashboard)
    response = requests.post(url, files=files, auth=auth)
    #print (response)

    return response

def delete_dashboard(host, port, user, password, account, dashboard):
    url = '{}:{}/controller/restui/dashboards/deleteDashboards'.format(host, port)
    data="[{}]".format(dashboard)
    #params = {'output': 'json'}
    headers = {
        'Content-Type' : 'application/json',
        'Accept' : 'application/json, text/plain, */*',
        'X-CSRF-TOKEN' : token
    }
    print('Delete Dashboard apps', dashboard)
    r = requests.post(url, headers=headers, data=data, cookies=cookies)
    # print(r.status_code )
    # print(r.text)
    if r.status_code > 299:
        print(r.text)
    return r


def create_widgets_image(widget_template, count):
    print('Creating image')
    widgets = []
    start_y = widget_template['y']
    start_x = widget_template['x']

    counter = 0
    for bt in bts:
        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x

        if "svg" not in new_widget['imageURL']: 
            new_widget['y'] = line_position * 200 + 20
        else:
            new_widget['y'] = line_position * 200 + start_y
    
        #print('@', new_widget['x'], new_widget['y'])
    
        widgets.append(deepcopy(new_widget))
        counter += 1
        #imageURL

    return widgets

def create_widgets_metric_label(widget_template, dashboards, bts):
    widgets = []
    start_y = widget_template['y']
    start_x = widget_template['x']
    counter = 0

    for bt in bts:
        bt = bt.replace("\n", "")
        print('Creating Metric label for', bt)
        dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))
        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x

        new_widget['y'] = line_position * 200 + start_y
        #print('@', new_widget['x'], new_widget['y'])
       
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        if dash_id != 0:
            new_widget["drillDownUrl"] = "{}:{}/controller/#/location=CDASHBOARD_DETAIL&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60&mode=MODE_DASHBOARD&dashboard={}".format(host, port, dash_id)
            new_widget["useMetricBrowserAsDrillDown"] = False
        
        widgets.append(deepcopy(new_widget))
        counter += 1
   
    return widgets

def create_widgets_labels(widget_template, dashboards, bts):
    #print('Creating Labels')
    widgets = []
    bt_ant=""
    counter = 0
    start_y = widget_template['y']
    start_x = widget_template['x']
    
    for bt in bts:
        bt = bt.replace("\n", "")
        print('Creating label for', bt)
        dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))
        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x
        
        if new_widget['borderEnabled']:
            new_widget['y'] = line_position * 200 + start_y
        else:
            if (new_widget['text'] == BTReplace) or new_widget['text'] == bt_ant:
                bt_ant = bt

                new_widget['text'] = bt
                new_widget['y'] = line_position * 200 + 20
            else:
                new_widget['y'] = line_position * 200 + start_y
        
        if dash_id != 0:
            new_widget["drillDownUrl"] = "{}:{}/controller/#/location=CDASHBOARD_DETAIL&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60&mode=MODE_DASHBOARD&dashboard={}".format(host, port, dash_id)
            new_widget["useMetricBrowserAsDrillDown"] = False
        
        widgets.append(deepcopy(new_widget))
        # print("new_widget=",new_widget)
        # print("widgets=",widgets)
        counter += 1

    return widgets


def create_widgets_hrs(application, widget_template, dashboards, bts, count):
    widgets = []
    start_y = widget_template['y']
    start_x = widget_template['x']
    counter = 0

    for bt in bts:
        print('Creating Health for', bt)
        bt = bt.replace("\n", "")
        dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))

        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x

        new_widget['y'] = line_position * 200 + start_y

        #new_widget['fontSize'] = 12
        if dash_id != 0:
            new_widget["drillDownUrl"] = "{}:{}/controller/#/location=CDASHBOARD_DETAIL&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60&mode=MODE_DASHBOARD&dashboard={}".format(host, port, dash_id)
            new_widget["useMetricBrowserAsDrillDown"] = False
            
        #print('@', new_widget['x'], new_widget['y'])

        new_widget["applicationReference"]["applicationName"] = application
        new_widget["applicationReference"]["entityName"] = application

        for entity in new_widget['entityReferences']:
            entity["applicationName"] = application
            if entity["entityName"].find("Response Time Health") != -1 :
                entity["entityName"] = "{}: Response Time Health".format(bt)
            if entity["entityName"].find("Error Rate Health") != -1 :
                entity["entityName"] = "{}: Error Rate Health".format(bt)
            if entity["entityName"].find("Calls") != -1 :
                entity["entityName"] = "{}: Calls".format(bt)

        widgets.append(deepcopy(new_widget))
        counter += 1
    
    return widgets


def create_widgets_metric(widget_template, dashboards, bts):
    widgets = []
    counter = 0
    start_y = widget_template['y']
    start_x = widget_template['x']

    for bt in bts:
        bt = bt.replace("\n", "")
        print('Creating metrics for', bt)
        dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))
        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x
        
        new_widget['y'] = line_position * 200 + start_y
        
        if dash_id != 0:
            new_widget["drillDownUrl"] = "{}:{}/controller/#/location=CDASHBOARD_DETAIL&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60&mode=MODE_DASHBOARD&dashboard={}".format(host, port, dash_id)
            new_widget["useMetricBrowserAsDrillDown"] = False

        #print('@', new_widget['x'], new_widget['y'])
        #print(new_widget)
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        widgets.append(deepcopy(new_widget))
        counter += 1
    return widgets

def create_widgets_pie(widget_template, dashboards, bts):
    widgets = []
    counter = 0
    start_y = widget_template['y']
    start_x = widget_template['x']

    for bt in bts:
        bt = bt.replace("\n", "")
        print('Creating pie for', bt)
        dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))
        new_widget = widget_template

        line_position = counter / WIDGETS_PER_LINE
        base_x = start_x + (counter % WIDGETS_PER_LINE) * x_offset
        new_widget['x'] = base_x

        new_widget['y'] = line_position * 200 + start_y
        
        if dash_id != 0:
            new_widget["drillDownUrl"] = "{}:{}/controller/#/location=CDASHBOARD_DETAIL&timeRange=last_1_hour.BEFORE_NOW.-1.-1.60&mode=MODE_DASHBOARD&dashboard={}".format(host, port, dash_id)
            new_widget["useMetricBrowserAsDrillDown"] = False

        #print('@', new_widget['x'], new_widget['y'])

        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][0]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        new_widget['dataSeriesTemplates'][1]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][1]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][1]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][1]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][1]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]
        
        new_widget['dataSeriesTemplates'][2]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][2]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][2]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][2]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][2]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        new_widget['dataSeriesTemplates'][3]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][3]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][3]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][3]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][3]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        new_widget['dataSeriesTemplates'][4]['metricMatchCriteriaTemplate']['applicationName'] = application
        new_widget['dataSeriesTemplates'][4]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['applicationName'] = application
        new_widget['dataSeriesTemplates'][4]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['entityName'] = bt
        new_widget['dataSeriesTemplates'][4]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['scopingEntityName'] = tiers[counter]
        new_widget['dataSeriesTemplates'][4]['metricMatchCriteriaTemplate']['entityMatchCriteria']['entityNames'][0]['subtype'] = types[counter]

        widgets.append(deepcopy(new_widget))
        counter += 1
    return widgets

def healthrules(health, bt, counter):
    linhas = ""
    
    for line in open('{}.xml'.format(health)).readlines(  ): 
        line = line.replace("Login: ", bt + ": ")
        if "<business-transaction application-component=" in line:
            line = "<business-transaction application-component=\"{}\">{}</business-transaction>".format(tiers[counter],bt)

        linhas = linhas + line
    file = open('{}_{}.xml'.format(health, counter), 'w') 
    file.write(linhas) 
    file.close() 
    url = '{}:{}/controller/healthrules/{}?overwrite=true'.format(host, port, appid)
    auth = ('{}@{}'.format(user, account), password)

    with open('{}_{}.xml'.format(health, counter), 'rb') as f:
        r = requests.post(url,  auth=auth, files={'blahblah': f})
        if "Imported 1 health rules successfully." not in r.text: 
            print("Error import Health Rule CALLS: {}".format(r.text))
    
    os.remove('{}_{}.xml'.format(health, counter))

def process_detail(dash, bt, numero):
    get_auth(host, port, user, password, account)
    
    dashboards = get_dashboards(host, port, user, password, account)
    dash_id = find_dashboard(dashboards, "{} - {}".format(application, bt))
    if dash_id != 0:
        response = delete_dashboard(host, port, user, password,account,dash_id)

    bts2 = [ bt ]
    new_dash = dash
    new_widgets = []
    count = 0
    new_dash['name'] = "{} - {}".format(application, bt)
    for widget in new_dash['widgetTemplates']:
        if widget['widgetType'] == 'HealthListWidget':
            new_widgets += create_widgets_hrs(application, widget, "", bts2, count)

        if widget['widgetType'] == 'TextWidget':
            new_widgets += create_widgets_labels(widget, "",  bts2)

        if widget['widgetType'] == 'GraphWidget':
            new_widgets += create_widgets_metric(widget, "", bts2)
        
        if widget['widgetType'] == 'ImageWidget':
            new_widgets += create_widgets_image(widget, bts2)

        if widget['widgetType'] == 'MetricLabelWidget':  
            new_widgets += create_widgets_metric_label(widget, "",  bts2) 
        if widget['widgetType'] == 'PieWidget':  
            new_widgets += create_widgets_pie(widget, "", bts2) 
    new_dash['widgetTemplates'] = new_widgets
    count += 1

    # print(json.dumps(new_dash, indent=4, sort_keys=True))
    host2=host.replace("http://", "")
    host2=host2.replace("https://", "")
    with open('new_dash_{}_{}_{}.json'.format(host2, application, numero), 'w') as outfile:
        json.dump(new_dash, outfile, indent=4, sort_keys=True)

    response = put_dashboard(host, port, user, password, account, 'new_dash_{}_{}_{}.json'.format(host2, application, numero))
    if (response.status_code != 200):
        print("Erro ao importar o Dashboard - {}-{}".format(application,bt))
    os.remove('new_dash_{}_{}_{}.json'.format(host2, application, numero))

def process(dash):
    get_auth(host, port, user, password, account)
    dashboards = get_dashboards(host, port, user, password, account)
    
    #APPS = get_applications(host, port, user, password, account)
    new_dash = dash
    new_widgets = []
    if WIDGETS_PER_LINE > 1:
        new_dash['width'] = x_offset * WIDGETS_PER_LINE + 30
    new_dash['height'] = 230 * len(bts) / WIDGETS_PER_LINE
    count = 0
    new_dash['name'] =  "Transacoes Criticas - " + application
    for widget in new_dash['widgetTemplates']:
        if widget['widgetType'] == 'HealthListWidget':
            new_widgets += create_widgets_hrs(application, widget, dashboards, bts, count)

        if widget['widgetType'] == 'TextWidget':
            new_widgets += create_widgets_labels(widget, dashboards, bts)

        if widget['widgetType'] == 'GraphWidget':
            new_widgets += create_widgets_metric(widget, dashboards, bts)
        
        if widget['widgetType'] == 'ImageWidget':
            new_widgets += create_widgets_image(widget, bts)

        if widget['widgetType'] == 'MetricLabelWidget':  
            new_widgets += create_widgets_metric_label(widget, dashboards, bts) 
        if widget['widgetType'] == 'PieWidget':  
            new_widgets += create_widgets_pie(widget, dashboards, bts) 
    new_dash['widgetTemplates'] = new_widgets
    count += 1

    # print(json.dumps(new_dash, indent=4, sort_keys=True))
    host2=host.replace("http://", "")
    host2=host2.replace("https://", "")
    with open('new_dash_{}_{}.json'.format(host2, application), 'w') as outfile:
        json.dump(new_dash, outfile, indent=4, sort_keys=True)
    
    response = put_dashboard(host, port, user, password, account, 'new_dash_{}_{}.json'.format(host2, application))
    if (response.status_code != 200):
        print("Erro ao importar o Dashboard - {}-{}".format(host2, application))
    os.remove('new_dash_{}_{}.json'.format(host2, application))


def main():
    global host
    global port
    global user
    global password
    global account
    global importacao
    global application
    global bts
    global tiers
    global types
    global appid
    global WIDGETS_PER_LINE

    # "height": 200 para cada BT
    now = datetime.datetime.now()
    print("===============================================  {}".format(now.strftime("%Y-%m-%d %H:%M")))

    if len(sys.argv) > 5:
        host = sys.argv[1] 
        port = sys.argv[2]
        user = sys.argv[3]
        password = sys.argv[4]
        account = sys.argv[5]
        application = sys.argv[6]

        if len(sys.argv) == 8 :
            importacao = sys.argv[7]
        if len(sys.argv) == 9 :
            WIDGETS_PER_LINE = int(sys.argv[8])

        bts = []
        tiers = []
        types = []

        for line in open('bts.txt').readlines(  ): 
            line = line.split(",")
            bt = line[1].replace("\n", "")
            tier = line[3].replace("\n", "")
            stype = line[0].replace("\n", "")
            stype = stype.replace("ASP.NET", "ASP_DOTNET")
            stype = stype.replace("Web Service", "ASP_DOTNET_WEB_SERVICE")
            stype = stype.replace(".NET Class / Method", "POCO")
            bts.append(bt)
            tiers.append(tier)
            types.append(stype)

        print("Total de BTs a processar: {}".format(len(bts)))
        print("-----------------------------------------------")

        APPS = get_applications(host, port, user, password, account)
        appid = 0
        for app in APPS:
            if (app['name'] == application):
                appid = app['id']

        if (importacao == '1'):
            counter = 0
            for bt in bts:
                print("Import Health Rule Calls: {}".format(bt))
                healthrules("Calls", bt, counter)
                print("Import Health Rule Response: {}".format(bt))
                healthrules("Response", bt, counter)
                print("Import Health Rule Error: {}".format(bt))
                healthrules("Error", bt, counter)
                counter += 1

        counter = 0
        for bt in bts:
            with open('dashboard_BT_Detail.json') as json_data:
                d = json.load(json_data)
                process_detail(d, bt, counter)
                counter += 1

        with open('dashboard.json') as json_data:
            d = json.load(json_data)
            process(d)
    else:
        print('dashboard.py <http(s)://host> <port> <user> <password> <account> <application_name> <criacao_HealthRule> <nro_widget_por_linha>')
        sys.exit(2)
    
    now = datetime.datetime.now()
    print("===============================================  {}".format(now.strftime("%Y-%m-%d %H:%M")))


if __name__ == '__main__':
    main()
